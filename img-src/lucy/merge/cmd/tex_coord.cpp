#include <iostream>

#include <ultimaille/all.h>

using namespace UM;

int main(int argc, char** argv) {
    if (3>argc) {
        std::cerr << "Usage: " << argv[0] << " model.obj model.obj" << std::endl;
        return 1;
    }

    Triangles m, n;
    read_by_extension(argv[1], m);
    read_by_extension(argv[2], n);


    vec3 bbmin, bbmax;
    n.points.util.bbox(bbmin, bbmax);

    double boxsize = 1., shrink = 1.;

    double maxside = std::max(bbmax.x-bbmin.x, bbmax.y-bbmin.y);
    for (int v : vert_iter(n))
        for (int d : range(2))
            n.points[v][d] = (n.points[v][d] - (bbmax[d]+bbmin[d])/2.)*boxsize/(shrink*maxside) + boxsize/2.;


    std::cerr << m.nverts() << " " << n.nverts() << std::endl;
    um_assert(m.nverts() == n.nverts());
    PointAttribute<vec2> tex_coord(m);
    for (int v : vert_iter(n)) {
        tex_coord[v] = {n.points[v].x, n.points[v].y};
    }

    write_by_extension("out.obj", m, { {{"tex_coord", tex_coord.ptr}}, {}, {}});
    return 0;
}
