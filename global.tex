


\section{Untangling global parameterizations}
% importance des param globales en computer graphics
An important application of parameterizations is the generation of quad meshes.
Given a 3D surface and its 2D flattening, applying the inverse of the map to a 2D grid generates a grid on the surface, i.e. a quad mesh.
Naturally, this technique requires the map to be locally invertibile, hence the importance of the untangling approach.
For producing more complex quad meshes~\cite{MIQ2009}, it is also possible to introduce discontinuities in the map.
To generate a valid quad mesh, however, we need to impose constraints along these discontinuities: the transition function that maps one side of a cut to the other side must be grid-preserving, i.e. it transforms the 2D unit grid onto itself (see Fig.~\ref{fig:transition}).

% nouvelle boundary condition
All such grid preserving transition functions can be decomposed into a rotation of $k\pi/2$ plus an integer translation.
In practice, producing such a global parameterization requires two parameterization steps: one where only the rotation is known (from a frame field e.g. \cite{Desobry2021}) and one where the translation is also known (after quantization e.g. \cite{IGM2013}).
In both cases, these boundary constraints are affine and can be easily introduced in our optimization scheme using \cite{Bommes:2010:PMO}.
Figure~\ref{fig:pipeline} illustrates the global parameterization pipeline.

\begin{figure}
    \centering
	    \includegraphics[width=.6\linewidth]{quads/fig0.jpg}
	\caption{The map is discontinuous across the red cut, but the projection of the unit grid cells from the map (right) to the object (left) coincide thanks to the grid preserving transition function (green arrow). }
	\label{fig:transition}
\end{figure}

\begin{figure*}
    \centering
	    \includegraphics[width=\linewidth]{PG_pipeline.jpg}
        \textbf{(a)}\hspace{32mm}
        \textbf{(b)}\hspace{32mm}
        \textbf{(c)}\hspace{32mm}
        \textbf{(d)}\hspace{32mm}
        \textbf{(e)}
	\caption{Quad generation via global parameterization pipeline.
\textbf{(a):} we compute a frame field over the input triangulation, it allows us to determine singular vertices.
Then  the mesh is cut open~\textbf{(b)} and flattened under grid preserving constraints along the cuts.
Least squares solution~\textbf{(c)} has inverted elements that we need to untangle~\textbf{(d)}.
Finally, the unit grid is projected back to the mesh to define the quad mesh~\textbf{(e)}.
}
\label{fig:pipeline}
\end{figure*}

% gaffe aux double covering
Recall that being free of inverted elements does not imply local invertibility~\cite[\S 3.3]{garanzha2021foldoverfree}.
In difficult cases, double coverings may appear. It is possible to prevent this with a brute force solution~\cite{garanzha2021b}.
Local injectivity can be enforced by adding extra (virtual) triangles. Unfortunately, this approach rigidifies the mesh and can be time consuming.
We can do better for global parameterizations.


\begin{figure}
\centering
\includegraphics[width=\linewidth]{covering.jpg}
\caption{A double covering example. \textbf{Left:} a surface to flatten is made of 12 equilateral triangles. \textbf{Middle:} the surface can be mapped to a plane without inverted elements, producing a double covering.
The map is not invertible in one point.
\textbf{Right:} gradients of the parameterization form two orthogonal vector fields (shown in red and blue), both of them have a -1 singularity at the center.
}
\label{fig:covering}
\end{figure}


We can interpret the gradients of the parameterization as a frame field \cite{NSDF2008}.
Double coverings arise from index -1 singularities of this field (refer to Fig.~\ref{fig:covering}).
Poincaré-Hopf theorem states that the sum of indices is constant, so an index $1$ singularity must be placed somewhere to compensate for the index $-1$.
With free boundary mapping (as in our example in Fig.~\ref{fig:covering}), the index $1$ is placed outside of the domain, simply adding a loop to the boundary.
For the global parameterization case, there is no free boundary, so the index $1$ must appear at a vertex.
Recall however, that our maps have positive Jacobian determinant over all elements, and thus it is impossible to place singularity 1 at a regular vertex, since it is a pole singularity that would force the map to have degenerate elements.
Therefore, the only possibility for the solver is to place it on a vertex that already has a negative index singularity (typically $-1/4$) as illustrated in Fig.~\ref{fig:promotion}.
\begin{figure}
\centering
\includegraphics[width=.9\linewidth]{quads/fig1.jpg}
\caption{
Quad mesh generation via global parameterization approach. The triangular domain is mapped to a plane, the green line corresponds to a (grid preserving) discontinuity in the map.
Red and blue lines correspond to the flat unit grid under the action of the inverse map.
\textbf{Left:} one singularity of index $-1/4$ is present in the domain, a valid quad mesh is generated.
\textbf{Right:} a double covering ($-1$ singularity) can appear if and only if the $-1/4$ singularity is ``promoted'' to $3/4$, thus total sum is still equal to -1/4.
The map is no longer invertbile (even if all elements have positive Jacobian!), leading to problems in quad mesh generation.
}
\label{fig:promotion}
\end{figure}

% solution for double covering
This observation allows to avoid double coverings by simply forcing vertices with negative index singularity to preserve the index.
To this end, for each such vertex $v$, we flatten its one ring, and compute the rotation + scale (with respect to $v$) that send each adjacent vertex to the next one.
The rotation is then scaled according to the index of the vertex ($\times 5/4$ for the index $-1/4$), and these affine equations tying in adjacent vertices are introduced as constraints to our system.
This solution forces the angle distortion to be perfectly distributed on the one ring of these vertices. A local mesh refinement is applied to prevent these new constraints to conflict.

% results
As illustrated on Fig.~\ref{fig:quads}, our method provides injective maps, even with the high distortion required to produce coarse quad meshes.

\begin{figure}
\centering
\includegraphics[width=.45\linewidth]{quads/42-param.jpg}
\includegraphics[width=.45\linewidth]{quads/42-quads.jpg}
\caption{To avoid double coverings, we constrain singularities with negative index and untangle the map (left).
This allows us to obtain valid parameterization even for coarsest quad layouts (right).}
\label{fig:quads}
\end{figure}


